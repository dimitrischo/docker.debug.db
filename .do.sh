#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

cd $DIR



if [ $1 == 'init' ]; then
if test -f ".config"; then
echo "Already initialized."
else
cd .DEFAULT
make updateplugins
docker-compose build
cd ..
touch .config
fi
elif [ $1 == 'update' ]; then
cd .DEFAULT
make updateplugins
elif [ $1 == 'new' ]; then
if test -f ".config"; then
if test -d /DATA/$2; then
echo Project already exists.
else
cp -R .DEFAULT DATA/$2
cd /DATA/$2
export cmdtmpcmd=s/COMPOSE_PROJECT_NAME=wordpress/COMPOSE_PROJECT_NAME=${2}/g
sed -i ${cmdtmpcmd} .env
export foundline=$(grep -n 'WORDPRESS_WEBSITE_TITLE' .env | cut -d: -f 1)
sed -i '/WORDPRESS_WEBSITE_TITLE=.*/d' .env
export cmdtmpcmd3="WORDPRESS_WEBSITE_TITLE=\'${3}\'"
sed -i "${foundline}s/.*/$cmdtmpcmd3/" .env
cd ../..
me="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
./$me start $2
cd /DATA/$2
make configure
fi
else
echo "do "init" first"
fi
elif  [ $1 == 'start' ]; then
if test -d /DATA/$2; then
me="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
./$me stop
cd /DATA/$2
docker-compose up -d
cd ../..
echo $2 > .config
else
echo Project does not exist, do "new `ProjectName` to create one."
fi
elif  [ $1 == 'stop' ]; then
if test -f ".config"; then
proj=$( cat .config )
cd /DATA/$proj
docker-compose stop
else
echo nothing to stop
fi
elif [ $1 == 'remove' ]; then
if [ $# -eq 0 ]; then
echo Please select a project like : remove "ProjectName"
else
cd /DATA/$2
docker-compose kill
docker-compose rm
cd ..
rm -rf $2
fi
else
echo Wrong command.
fi

