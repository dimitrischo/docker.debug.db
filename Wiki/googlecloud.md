# How to upload on Google Cloud

### Step 1: Create a new bucket. (DO ONCE per wp project) 
1. Go to **https://console.cloud.google.com**
2. Select your Google Cloud project (Top blue toolbar)
3. At left sidebar select **STORAGE** -> **Storage**
4. At top side, Click on "CREATE BUCKET"
5. __IMPORTANT__: Read the following ***Create Bucket options*** section
6. Select your preferred  options
7. **Choose where to store your data** , See **Learn more** to help you choose.
8. **Choose a default storage class for your data** -> **Standard**
9. **Choose how to control access to objects** -> **Uniform**
10. **Advanced settings (optional)** -> **Google-managed key**
11. ***CREATE***
12. Click **PERMISSIONS** at the Horizontal bar
13. Click **ADD**
14. At **New members** field type: **allUsers**
15. At **Select a role** field choose: **Cloud Storage**-> **Storage Object Viewer**
15. Click **SAVE**
16. Click **ALLOW PUBLIC ACCESS**
You are ready to browse at your bucket and upload your files. (you can drag and drop)



### Step 2: Upload files. (Repeat for each deployment)
2. Browse to your bucket (Help yourself from Step 1)
3. Drag and drop your files at the browser's *File Browser* section the contents from  
***You will find the generated files at "ProjectName/wordpress/wp-content/uploads/wp2static-processed-site/"***

### Create Bucket options
##### Option 1: Use Google's subdomain
For more details please visit : ***Naming guidelines***
This is the default option if your selected name does not contain any dots.
Google's subdomain for buckets is: storage.googleapis.com
You will find your files at: https://***YourBucketName***.storage.googleapis.com/

##### Option 2: Use owned domain name/subdomain
For more details please visit : ***Naming guidelines***
This option requires domain name ownership verification.
See more at: https://cloud.google.com/storage/docs/hosting-static-website
You will find your files at the specified domain name.
