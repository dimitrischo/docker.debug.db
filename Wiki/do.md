### AVAILABLE COMMANDS
##### Initialization
```sh
./do.sh init
```
##### Update wp custom plugins
```sh
./do.sh update
```
##### New project
```sh
# USAGE: new {ProjectName} ?{WebsiteTitle}
./do.sh new blog
./do.sh new blog MyNewBlog
./do.sh new blog "My New Blog"
```
##### Start
*No need to "stop" before "start"
```sh
./do.sh start blog
```
##### Resume
```sh
./do.sh resume
```
##### Stop
*Stops the running project containers
```sh
./do.sh stop
```
##### Remove
```sh
./do.sh remove blog
```