# How to Generate a static site.

### Step 1: Configure. (DO ONCE per wp project) 
1. Click **WP2Static** menu at left side.
2. Click **Options**
3. Fill **Deployment URL** with the url for your generated & processed static website.

### Step 2: Generate. (Repeat for each deployment)
1. Click **WP2Static** menu at left side.
2. Click **Run**
3. Click ***Generate static site***
You will find the generated files at "ProjectName/wordpress/wp-content/uploads/wp2static-processed-site/"

#### See also
- [How to upload on Google Cloud](/Wiki/googlecloud.md)