# Summary

## Prerequirements
 - Install [docker]
 - Install [docker-compose]

# How to use:
This section has 3 parts.
- Part 1 is about steps you have to follow once on a new host machine.
- Part 2 contains the general process you have to go trough each time you want a new wordpress project in your playground.
- Part 3 some daily commands you should use.
- Part 4 has details about what happened. \
*Quick demo video can be found at [DemoVideo](/Wiki/Docker_WPSTATIC.mp4).
### Part 1: TO DO ONCE
_________________
##### 1. Download the git repo from command line or a browser.
For command line replace ***username*** and ***password*** :
````sh
git clone https://username:password@gitlab.com/engaging/wordpress-flatten-docker.git
````
##### 2. Browse to the downloaded path.
*Unzip first in case of desktop download.
````sh
cd wordpress-flatten-docker
````
##### 3. Give execution permissions to the main script.
````sh
chmod +x do.sh
````
##### 4. Initialize.
````sh
./do.sh init
````

### Part 2: Repeat for each wp project
_________________
##### 5. Create a new wordpress project.
Replace ***blog*** with your name for this project. \
Replace ***My New Blog*** with your website title. \
*You can skip the title by not passing the parameter at all, see more at [do](do.md) wiki
````sh
./do.sh new blog "My New Blog"
````
After that you should be able to access your new wordpress site at: http://localhost

### Part 3: Daily Routine
_________________
- Change running project
    ````sh
    ./do.sh start ProjectName
    ````
    That will stop the running wp project and start the other one for you.

- Pause everything. **NOTE: This is what you do if you need to stop the containers.**
    ````sh
    ./do.sh pause
    ````
- Resume the last active container. **NOTE: This is what you do if you need to start doing some work again.**
    ````sh
    ./do.sh resume
    ````
    Be sure to do that when you are done with your work.
- Remove a project
    ````sh
    ./do.sh remove ProjectName
    ````
    Be sure to do that when you are done with your work.

### Part 4: Go Deep
_________________
##### Important!
Use http://localhost NOT http://127.0.0.1 for wordpress to work properly.

##### Ports :
The following ports will be opened for local connections. \
**80**: Wordpress, http://localhost \
**8080**: PhpMyadmin, http://localhost:8080 \
**3306**: Database (MariaDB), (you have to know what you are doing here :) )

##### Useful directories :
"./do.sh new ProjectName" creates a new directory called ./DATA/*ProjectName*. \
In that directory you will find ***wordpress*** and ***mysql*** directories. \
There you can find all useful data. Have fun with it.
##### DO NOT TOUCH! :
Seriously! This is a fragile ecosystem do not edit any of the executable code if you are not 100% sure of its purpose and do not edit any configuration file if you don't know  what you are doing.

##### The magic :
~To be continued~


#### See also
- [How to Generate a static site](/Wiki/wp2static.md)
- [How to upload on Google Cloud](/Wiki/googlecloud.md)


[docker]: <https://docs.docker.com/engine/install/>
[docker-compose]: <https://docs.docker.com/compose/install/>
