#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

cd $DIR



if [ $1 == 'init' ]; then
docker-compose up -d --build
docker exec -it WPDockerfy bash -c "chmod +x /do.sh && ./do.sh init"
elif [ $1 == 'update' ]; then
docker-compose up -d --build
docker exec -it WPDockerfy bash -c "./do.sh update"
elif [ $1 == 'new' ]; then
docker exec -it WPDockerfy bash -c "./do.sh new $2 \"${3}\""
elif  [ $1 == 'start' ]; then
docker-compose up -d
docker exec -it WPDockerfy bash -c "./do.sh start $2"
elif  [ $1 == 'resume' ]; then
docker-compose unpause
elif  [ $1 == 'stop' ]; then
docker-compose pause
elif [ $1 == 'remove' ]; then
docker exec -it WPDockerfy bash -c "./do.sh remove $2"
else
echo Wrong command.
fi

