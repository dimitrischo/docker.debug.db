#!/bin/bash

mkdir -p wp_plugins
cd wp_plugins
rm -rf wp2static-addon-zip
git clone https://github.com/leonstafford/wp2static-addon-zip.git
cd wp2static-addon-zip
docker run --rm --interactive --tty \
  --volume $PWD:/app \
  composer install
