#!/bin/bash

#./tools/set_all_env_vars.sh
cat .env | grep "^[^#;]" | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\nexport /g' > out_tmp.sh
sed  -i '1s/^/export /' out_tmp.sh
source out_tmp.sh
rm out_tmp.sh

chown -R 33:33 ./tools/
chown -R 33:33 ${WORDPRESS_DATA_DIR:-./wordpress}
docker-compose -f docker-compose.yml -f wp-auto-config.yml run --rm wp-auto-config
docker run --rm --volumes-from ${COMPOSE_PROJECT_NAME:-wordpress} --network container:${COMPOSE_PROJECT_NAME:-wordpress} wpcli wp plugin activate wp2static