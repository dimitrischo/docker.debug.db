#!/bin/bash

mkdir -p wp_plugins
cd wp_plugins
rm -rf wp2static
git clone https://github.com/leonstafford/wp2static.git
cd wp2static

##/var/run/docker.sock used
#docker run --rm --interactive \
#  --volume $PWD:/app \
#  composer install

docker run --rm --interactive --tty \
  --volume $PWD:/app \
  composer install
