# Introduction
The current project is made so end users can deploy containerized wordpress playgrounds in a convenient way. \
Only 1 wordpress site can be active at time! \
Please read more at [Wiki](/Wiki/README.md)

## Prerequirements
 - Install [docker]
 - Install [docker-compose]

### System requirements:
- 1.5 Gb disk space *MINIMUM*
- 150 Mb additional per each wp project.



## IMPORTANT
There is a "USE_AT_YOUR_OWN_RISK.sh" script that wipes docker and downloads the project, review and edit before use.

[docker]: <https://docs.docker.com/engine/install/>
[docker-compose]: <https://docs.docker.com/compose/install/>
